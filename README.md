# Urn-Pong

This is an implementation of Pong written in [Urn](http://urn-lang.com) using [urn-love2d-bindings](https://gitlab.com/Lignum/urn-love2d-bindings/tree/master).

## Building

To build the game, all you need to do is:
```Bash
urn pong.lisp -o main
```

This will output a main.lua file, meaning you can simply run the game with:
```Bash
love .
```

