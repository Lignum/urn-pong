-- It's easier to just write this in Lua.
function love.conf(t)
	t.identity = "urn_pong"

	t.window.width = 1280
	t.window.height = 720
	t.window.title = "Pong in Urn"
	t.window.fullscreen = false
	t.window.resizable = false
	t.window.msaa = 4

	t.modules.physics = false
	t.modules.joystick = false
	t.modules.image = false
	t.modules.sound = false
	t.modules.thread = false
end
