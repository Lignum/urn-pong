(import love/love (defevent))
(import love/graphics)
(import love/math love/maths)
(import love/keyboard)

(import lua/os os)
(import lua/math maths)

(define max maths/max)
(define min maths/min)

(defun centre-coord (cs ls)
  (- (/ cs 2) (/ ls 2)))

(defun random-negate (rand)
  (if (= (self rand :random 0 1) 1) 1 -1))

(defun distance (ax ay bx by)
  (let [(x (- bx ax))
        (y (- by ay))]
    (math/abs (math/sqrt (+ (* x x) (* y y))))))

(defun point-in-circle (x y cx cy r)
  (<= (distance x y cx cy) r))

(defun point-in-rectangle (x y rx ry w h)
  (and (>= x rx) (>= y ry) (<= x (+ rx w)) (<= y (+ ry h))))

(defun point (x y)
  { :x x :y y })

(defun circle-in-rectangle (x y r rx ry w h)
  (with (verts `(,(point rx ry) ,(point (+ rx w) ry) ,(point rx (+ ry h)) ,(point (+ rx w) (+ rx h))))
    (or (point-in-rectangle x y rx ry w h) (any (lambda (p)
                                                  (point-in-circle (.> p :x) (.> p :y) x y r)) verts))))

(define paddleWidth 16)
(define paddleHeight 128)
(define paddleVelocity 256)
(define ballVelocity 512)
(define ballRadius 8)

(defun lerp (x y t)
  (+ (* (- 1 t) x) (* t y)))

(defun reflect-ball! (ball paddle)
  (.<! ball :vx (- 0 (.> ball :vx)))
  (let* [(paddleTopY (.> paddle :y))
         (by (- (.> ball :y) paddleTopY))
         (ny (maths/rad (lerp -45 45 (/ by paddleHeight))))
         (vx (.> ball :vx)) (vy (.> ball :vy))
         (mag (maths/sqrt (* vx vx) (* vy vy)))]
    (.<! ball :vy (* (maths/sin ny) mag))))

(defun respawn-ball! (rand ball)
  (let [(sw (love/graphics/get-width))
        (sh (love/graphics/get-height))]
    (.<! ball :x (centre-coord sw ballRadius))
    (.<! ball :y (centre-coord sh ballRadius))
    (let* [(maxAngle (maths/deg (maths/asin (/ sh (maths/sqrt (+ (* sw sw) (* sh sh)))))))
           (minAngle (- 0 maxAngle))
           (angle (maths/rad (self rand :random minAngle maxAngle)))]
        (.<! ball :vx (* (maths/cos angle) ballVelocity (random-negate rand)))
        (.<! ball :vy (* (maths/sin angle) ballVelocity)))))

(let* [(ball { :x 0 :y 0 :vx 0 :vy 0 }) 
       (paddleLeft { :x 0 :y 0 }) (paddleRight { :x 0 :y 0 })
       (scoreLeft 0) (scoreRight 0)
       (rand nil)]
  (defevent :load ()
    (let* [(sw (love/graphics/get-width))
           (sh (love/graphics/get-height))
           (pad 8)
           (paddleY (centre-coord sh paddleHeight))]
      (.<! paddleLeft :x pad)
      (.<! paddleLeft :y paddleY)
      (.<! paddleRight :x (- sw paddleWidth pad))
      (.<! paddleRight :y paddleY)
      (set! rand (love/maths/new-random-generator))
      (self rand :setSeed (os/time))
      (respawn-ball! rand ball))) ;; for president 2016

  (defevent :draw ()
    (love/graphics/rectangle "fill" (.> paddleLeft :x) (.> paddleLeft :y) paddleWidth paddleHeight)
    (love/graphics/rectangle "fill" (.> paddleRight :x) (.> paddleRight :y) paddleWidth paddleHeight)
    (love/graphics/circle "fill" (.> ball :x) (.> ball :y) ballRadius)
    (let* [(scoreTextLeft (.. "Score: " scoreLeft))
           (scoreTextRight (.. "Score: " scoreRight))
           (font (love/graphics/get-font))
           (scoreRightWidth (self font :getWidth scoreTextRight))
           (sw (love/graphics/get-width))]
      (love/graphics/print scoreTextLeft 4 4 0 2 2)
      (love/graphics/print scoreTextRight (- sw (* scoreRightWidth 2) 4) 4 0 2 2)))

  (defevent :update (dt)
    ;; Get user input
    (let* [(s (* paddleVelocity dt))
           (ry (.> paddleRight :y))
           (ly (.> paddleLeft :y))
           (sh (love/graphics/get-height))
           (f (- sh paddleHeight))] ;;; The greatest y value a paddle may assume
      (cond
        [(love/keyboard/is-down "up")
         (.<! paddleRight :y (max 0 (- ry s)))]
        [(love/keyboard/is-down "down")
         (.<! paddleRight :y (min f (+ ry s)))]
        [true])
      (cond
        [(love/keyboard/is-down "w")
         (.<! paddleLeft :y (max 0 (- ly s)))]
        [(love/keyboard/is-down "s")
         (.<! paddleLeft :y (min f (+ ly s)))]
        [true]))

    ;; Check for ball for collisions 
    (let [(x (.> ball :x))
          (y (.> ball :y))]
      (cond
        ;;; Ball collides with paddles
        [(circle-in-rectangle x y ballRadius (.> paddleLeft :x) (.> paddleLeft :y) paddleWidth paddleHeight)
         (reflect-ball! ball paddleLeft)]
        [(circle-in-rectangle x y ballRadius (.> paddleRight :x) (.> paddleRight :y) paddleWidth paddleHeight)
         (reflect-ball! ball paddleRight)]
        ;;; Ball collides with bottom/top of the screen
        [(or (<= y 0) (>= y (love/graphics/get-height)))
         (.<! ball :vy (- 0 (.> ball :vy)))] 
        ;;; Ball is outside the screen horizontally, score
        [(< x 0)
         (inc! scoreRight)
         (respawn-ball! rand ball)]
        [(> x (love/graphics/get-width))
         (inc! scoreLeft)
         (respawn-ball! rand ball)]
        ;;; No collision, do nothing
        [true]))

    ;; Update the ball's position
    (let* [(x (.> ball :x)) (vx (.> ball :vx))
           (y (.> ball :y)) (vy (.> ball :vy))
           (nx (+ x (* vx dt))) (ny (+ y (* vy dt)))]
      (.<! ball :x nx)
      (.<! ball :y ny))))
